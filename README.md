## Frontend Dockerfile
```dockerfile
# prepare build env
FROM node:20.12.0 AS build
WORKDIR /build
RUN npm cache clean --force
COPY . .
RUN npm install 
RUN npm run build --prod

# prepare app env
FROM nginx:latest
COPY --from=build /build/dist/angular-15-crud /usr/share/nginx/html
COPY /nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
```
## Frontend commands:
```bash
cd frontend
docker build -t frontend:v1.0.0 .
docker run -p 4000:80 --name frontend frontend:v1.0.0
```

## Backend Dockerfile
```dockerfile
## prepare build env
FROM maven:3.9.6-eclipse-temurin-17-alpine AS build
WORKDIR /build
COPY . .
RUN mvn clean package

## prepare app env
FROM openjdk:17
WORKDIR /app
COPY --from=build /build/target/backend.jar backend.jar
ENTRYPOINT ["java", "-jar", "/app/backend.jar"]
EXPOSE 9091
```

## Backend commands:
```bash
cd backend
docker build -t backend:v1.0.0 .
docker run -p 9091:9091 --name backend backend:v1.0.0
```
